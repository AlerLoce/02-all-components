import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Clock from './Clock/Clock';
import MouseTracker2 from './MouseTracker/MouseTracker';
import Events from './Events/Events';
import Toggle from './Toggle/ToggleContainer';
import Greeting from './Greeting/Greeting';
import Form from './Form/Form';

import * as serviceWorker from './serviceWorker';

//ReactDOM.render(<App2 />, document.getElementById('root'));

function formatName(user) {
  return user.firstName + ' ' + user.lastName;
}

const user = {
  firstName: 'Harper',
  lastName: 'Perez'
};

const element2 = App();//<Welcome name="Sara" />;

function Welcome(props) {
  return <h1>... Hello, {props.name}</h1>;
}

function App() {
  return (
    <div>
      <Welcome name="Sara" />
      <Welcome name="Cahal" />
      <Welcome name="Edite" />
    </div>
  );
}

////////////////////////////////////////////////

  ////////////////////////////////////////////////

const element = (
  <h1>
    Hello, {formatName(user)}!
    {element2}
    <Form />
    <Clock />
    <MouseTracker2 />
    <Events />
    <Toggle />
    <Greeting />
  </h1>
);


/*
// function tick() {
//   const element = (
//     <div>
//       <h1>Hello, world!</h1>
//       <h2>It is {new Date().toLocaleTimeString()}.</h2>
//     </div>
//   );
//   ReactDOM.render(element, document.getElementById('root'));
// }
*/



ReactDOM.render(
  element,
  document.getElementById('root')
);


// ReactDOM.render(
//   element2,
//   document.getElementById('Welcome')
// );


//setInterval(tick, 1000);
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
