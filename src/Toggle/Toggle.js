import React  from 'react';


function Toggle(props) {

    return (
      <button onClick={props.handleClick} >
        {props.isToggleOn ? 'ON' : 'OFF'}
      </button>
    );

}

// module.exports = Toggle;
 export default Toggle