import React , { Component } from 'react';
import Toggle from './Toggle';

class ToggleContainer extends Component {

  constructor(props) {
    super(props);
    this.state = {isToggleOn: true};

    // This binding is necessary to make `this` work in the callback
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState(state => ({
      isToggleOn: !state.isToggleOn
    }));
  }

	render () {
		return (
			<Toggle 
				isToggleOn={ this.state.isToggleOn }
				handleClick={ this.handleClick }
			/>
			);
	}
}

export default ToggleContainer 