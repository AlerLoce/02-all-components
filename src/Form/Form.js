import React , { Component } from 'react';

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {nombre: '', apellido:''};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    console.log(event.target.name)
    this.setState(
      {[event.target.name]: event.target.value}
    );
  }

  handleSubmit(event) {
    alert(`A name was submitted: ${this.state.nombre} ${this.state.apellido}`)
    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          Name:
          <input type="text" name='nombre' value={this.state.nombre} onChange={this.handleChange} />
          Apellido:
          <input type="text" name='apellido' value={this.state.apellido} onChange={this.handleChange} />
        </label>
        <input type="submit" value="Submit" />
        
      </form>
    );
  }
}


export default Form 