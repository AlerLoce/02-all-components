import React , { Component } from 'react';
//port Events from './Events/Events';

class Greeting extends Component {
  constructor(props) {
    super(props);
    this.state = {isToggleOn: false};

    // This binding is necessary to make `this` work in the callback
    this.handleClick = this.handleClick.bind(this);
  }

   UserGreeting(props) {
  	return <h1>Welcome back!</h1>;
  }
   GuestGreeting(props) {
  	return <h1>Please sign up.</h1>;
  }
   handleClick(e, props) {
    this.setState(state => ({
      isToggleOn: !state.isToggleOn
    }));
  }
   Greeting2(props) {
  	const isToggleOn = props.isToggleOn;
  	if (isToggleOn) {
  		return this.UserGreeting;
  	}
  	return ( 
  			this.GuestGreeting
  		);
  }
  

  render() {
  	//const  isToggleOn = this.state.isToggleOn;
  	let Greet;
  	Greet = this.Greeting2;
    return (
    	<div>
    	{Greet}
      	<button onClick={this.handleClick}>
        	{this.state.isToggleOn ? 'isLoged' : 'pls Log'}
      	</button>

      	
      	</div>
    );
  }
}


export default Greeting