import React, { Component } from 'react';
import './MouseTracker.css';

class MouseTracker extends Component {

	constructor(props) {
		super(props);
		this.handleMouseMove = this.handleMouseMove.bind(this);
		this.state = { x:0, y:0 };
	}

	handleMouseMove(e) {
		this.setState( {
			x: e.clientX,
			y: e.clientY
		} )
	}
 
	render () {
		return (
			<div className="MouseTracker" style={{ height:'100%' }} onMouseMove={this.handleMouseMove}>
				<h1>Move the mouse around!</h1>
				<p>Te current mouse position is ({this.state.x}, {this.state.y})</p>
			</div>
		)
	}
}

class MouseTracker2 extends React.Component {
  render() {
    return (
      <div>
        <h1>Move the mouse around! trck2</h1>
        <MouseTracker />
      </div>
    );
  }
}

export default MouseTracker2
